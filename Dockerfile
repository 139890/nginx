FROM nginx:alpine

MAINTAINER Tomas Sapak <tom.sapak@gmail.com>

EXPOSE 80 443
